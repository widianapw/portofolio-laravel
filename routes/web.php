<?php

use App\Http\Livewire\Admin\User;
use App\Http\Livewire\Admin\Award;
use App\Http\Livewire\Admin\Skill;
use App\Http\Livewire\Admin\Project;
use Illuminate\Support\Facades\Route;
use App\Http\Livewire\Admin\Education;
use App\Http\Livewire\Admin\Experience;
use App\Http\Livewire\Portofolio\Index;
use App\Http\Livewire\Admin\Index as AdminIndex;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', Index::class);

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

Route::middleware(['auth'])->prefix('admin')->as('admin.')->group(function () {
    Route::get('/', User::class)->name('index');
    Route::get('/user', User::class)->name('user');
    Route::get('/education', Education::class)->name('education');
    Route::get('/skill', Skill::class)->name('skill');
    Route::get('/award', Award::class)->name('award');
    Route::get('/experience', Experience::class)->name('experience');
    Route::get('/project', Project::class)->name('project');
});
require __DIR__ . '/auth.php';
