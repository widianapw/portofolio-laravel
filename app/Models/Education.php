<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Education extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function getDateRangeAttribute()
    {
        $format = "F Y";
        $dateFrom = date_format(date_create($this->date_from), $format);
        $dateUntil = is_null($this->date_until) ? "Present" : date_format(date_create($this->date_until), $format);
        return $dateFrom . ' - ' . $dateUntil;
    }
}
