<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Education as ModelsEducation;

class Education extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $education, $is_present = false;
    protected $rules = [
        'education.organization' => 'required',
        'education.majority' => 'required',
        'education.date_from' => 'required',
        'education.date_until' => '',
    ];
    protected $listeners = ['confirm' => 'deleteProcess'];

    public function render()
    {
        return view('livewire.admin.education', [
            'educations' => ModelsEducation::where('user_id', auth()->id())->latest()->paginate(5)
        ]);
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function mount()
    {
        $this->education = new ModelsEducation();
    }

    public function store()
    {
        $this->validate();
        $this->education['user_id'] = auth()->id();
        $this->education['date_until'] = $this->is_present ? null : $this->education['date_until'];
        $this->education->save();
        $this->showSuccessDialog("Saved");
        $this->reset();
        $this->mount();
    }

    public function delete($id)
    {
        $this->dispatchBrowserEvent('swal:confirm', [
            'type' => 'warning',
            'title' => 'Delete?',
            'message' => 'Are you sure want to delete this?',
            'id' => $id
        ]);
    }

    public function deleteProcess($id)
    {
        ModelsEducation::find($id)->delete();
        $this->showSuccessDialog("Deleted");
    }
}
