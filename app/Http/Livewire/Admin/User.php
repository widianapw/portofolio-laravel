<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\User as ModelsUser;
use Illuminate\Support\Facades\Storage;

class User extends Component
{
    use WithFileUploads;
    public $user;
    public $image;
    protected $rules = [
        'user.first_name' => 'min:5',
        'user.last_name' => 'min:5',
        'user.full_name' => 'min:5',
        'user.phone' => 'min:5',
        'user.address' => 'min:5',
        'user.about' => 'min:5',
        'user.github_url' => 'min:5',
        'user.facebook_url' => 'min:5',
        'user.twitter_url' => 'min:5',
        'user.linked_in_url' => 'min:5',
    ];
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function render()
    {
        return view('livewire.admin.user', []);
    }

    public function mount()
    {
        $this->user =
            ModelsUser::find(auth()->id())->first();
    }

    public function updateUser()
    {
        $this->validate();

        if ($this->image) {
            $imageName = uniqid() . '.png';
            $this->image->storeAs('public/user', $imageName);
            $this->user['image_url'] = Storage::url('public/user/' . $imageName);
        }

        $this->user->update();
        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'success',
            'message' => 'Successfully edited.'
        ]);
        $this->reset();
        $this->mount();
    }
}
