<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Skill as ModelsSkill;

class Skill extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $skill;

    protected $listeners = ['confirm' => 'deleteProcess'];

    protected $rules = [
        'skill.skill' => 'required|min:5'
    ];
    public function render()
    {
        return view('livewire.admin.skill', [
            'skills' => ModelsSkill::where('user_id', auth()->id())->latest()->paginate(5)
        ]);
    }

    public function mount()
    {
        $this->skill = new ModelsSkill();
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function store()
    {
        $this->validate();
        ModelsSkill::create([
            'user_id' => auth()->id(),
            'skill' => $this->skill->skill
        ]);
        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'success',
            'message' => 'Saved successfully.'
        ]);

        $this->reset();
        $this->mount();
    }

    public function delete($id)
    {
        $this->dispatchBrowserEvent('swal:confirm', [
            'type' => 'warning',
            'title' => 'Delete?',
            'message' => 'Are you sure want to delete this?',
            'id' => $id
        ]);
    }

    public function deleteProcess($id)
    {
        ModelsSkill::find($id)->delete();
        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'success',
            'message' => 'Deleted successfully'
        ]);
    }
}
