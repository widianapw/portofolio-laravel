<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Project as ModelsProject;

class Project extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $project;
    protected $listeners = ['confirm' => 'deleteProcess'];
    protected $rules = [
        'project.name' => 'required',
        'project.technology' => 'required',
        'project.description' => 'required'
    ];
    public function render()
    {
        return view('livewire.admin.project', [
            'projects' => ModelsProject::where('user_id', auth()->id())->latest()->paginate(5)
        ]);
    }

    public function mount()
    {
        $this->project = new ModelsProject();
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function store()
    {
        $this->validate();
        $this->project['user_id'] = auth()->id();
        $this->project->save();
        $this->showSuccessDialog("Saved");
        $this->reset();
        $this->mount();
    }

    public function delete($id)
    {
        $this->dispatchBrowserEvent('swal:confirm', [
            'type' => 'warning',
            'title' => 'Delete?',
            'message' => 'Are you sure want to delete this?',
            'id' => $id
        ]);
    }

    public function deleteProcess($id)
    {
        ModelsProject::find($id)->delete();
        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'success',
            'message' => 'Deleted successfully'
        ]);
    }
}
