<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Experience as ModelsExperience;

class Experience extends Component
{


    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $experience, $is_present = false;
    protected $listeners = ['confirm' => 'deleteProcess'];
    protected $rules = [
        'experience.position' => 'required',
        'experience.company' => 'required',
        'experience.description' => 'required',
        'experience.date_from' => 'required',
        'experience.date_until' => 'required',
    ];
    public function render()
    {
        return view('livewire.admin.experience', [
            'experiences' => ModelsExperience::where('user_id', auth()->id())->latest()->paginate(5)
        ]);
    }

    public function mount()
    {
        $this->experience = new ModelsExperience();
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function store()
    {
        $this->validate();
        $this->experience['user_id'] = auth()->id();
        $this->experience['date_until'] = $this->is_present ? null : $this->experience['date_until'];
        $this->experience->save();
        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'success',
            'message' => 'Saved successfully.'
        ]);

        $this->reset();
        $this->mount();
    }

    public function delete($id)
    {
        $this->dispatchBrowserEvent('swal:confirm', [
            'type' => 'warning',
            'title' => 'Delete?',
            'message' => 'Are you sure want to delete this?',
            'id' => $id
        ]);
    }

    public function deleteProcess($id)
    {
        ModelsExperience::find($id)->delete();
        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'success',
            'message' => 'Deleted successfully'
        ]);
    }
}
