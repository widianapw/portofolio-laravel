<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Award as ModelsAward;

class Award extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $award;
    protected $listeners = ['confirm' => 'deleteProcess'];
    protected $rules = ['award.award' => 'min:5'];
    public function render()
    {
        return view('livewire.admin.award', [
            'awards' => ModelsAward::where('user_id', auth()->id())->paginate(5)
        ]);
    }

    public function mount()
    {
        $this->award = new ModelsAward();
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function store()
    {
        $this->validate();
        ModelsAward::create([
            'user_id' => auth()->id(),
            'award' => $this->award->award
        ]);
        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'success',
            'message' => 'Saved successfully.'
        ]);

        $this->reset();
        $this->mount();
    }

    public function delete($id)
    {
        $this->dispatchBrowserEvent('swal:confirm', [
            'type' => 'warning',
            'title' => 'Delete?',
            'message' => 'Are you sure want to delete this?',
            'id' => $id
        ]);
    }

    public function deleteProcess($id)
    {
        ModelsAward::find($id)->delete();
        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'success',
            'message' => 'Deleted successfully'
        ]);
    }
}
