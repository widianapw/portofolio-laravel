<?php

namespace App\Http\Livewire\Portofolio;

use App\Models\User;
use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.portofolio.index', [
            'user' => User::find(1)->first()
        ])->layout('layouts.portofolio1');
    }
}
