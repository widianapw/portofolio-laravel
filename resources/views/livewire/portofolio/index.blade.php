<div>
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">
            <span class="d-block d-lg-none">{{ $user->firstLastName }}</span>
            <span class="d-none d-lg-block"><img class="img-fluid rounded-circle mx-auto mb-2 border border-lightgit"
                    style="max-width: 10rem; max-height: 10rem" src="{{ $user->image_url }}" alt="..." /></span>
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive"
            aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span
                class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav">
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#about">About</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#experience">Experience</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#education">Education</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#skills">Skills</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#projects">Projects</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#awards">Awards</a></li>
            </ul>
        </div>
    </nav>
    <!-- Page Content-->
    <div class="container-fluid p-0">
        <!-- About-->
        <section class="resume-section" id="about">
            <div class="resume-section-content">
                <h1 class="mb-0">
                    {{ $user->first_name }}
                    <span class="text-primary">{{ $user->last_name }}</span>
                </h1>
                <div class="subheading mb-5">
                    {{ $user->address }} · {{ $user->phone }} ·
                    <a href="mailto:{{ $user->email }}">{{ $user->email }}</a>
                </div>
                <p class=" lead mb-5">{{ $user->about }}</p>
                <div class="social-icons">
                    <a class="social-icon" href="{{ $user->linked_in_url }}"><i class="fab fa-linkedin-in"></i></a>
                    <a class="social-icon" href="{{ $user->github_url }}"><i class="fab fa-github"></i></a>
                    <a class="social-icon" href="{{ $user->twitter_url }}"><i class="fab fa-instagram"></i></a>
                    <a class="social-icon" href="{{ $user->facebook_url }}"><i class="fab fa-facebook-f"></i></a>
                </div>
            </div>
        </section>
        <hr class="m-0" />
        <!-- Experience-->
        <section class="resume-section" id="experience">
            <div class="resume-section-content">
                <h2 class="mb-5">Experience</h2>
                @foreach ($user->experience->sortDesc() as $item)
                    <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
                        <div class="flex-grow-1">
                            <h3 class="mb-0">{{ $item->position }}</h3>
                            <div class="subheading mb-3">{{ $item->company }}</div>
                            <p>{{ $item->description }}</p>
                        </div>
                        <div class="flex-shrink-0"><span class="text-primary">{{ $item->dateRange }}</span></div>
                    </div>
                @endforeach
            </div>
        </section>
        <hr class="m-0" />
        <!-- Education-->
        <section class="resume-section" id="education">
            <div class="resume-section-content">
                <h2 class="mb-5">Education</h2>
                @foreach ($user->education->sortDesc() as $item)
                    <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
                        <div class="flex-grow-1">
                            <h3 class="mb-0">{{ $item->organization }}</h3>
                            <div class="subheading mb-3">{{ $item->majority }}</div>
                            {{-- <div>Computer Science - Web Development Track</div> --}}
                        </div>
                        <div class="flex-shrink-0"><span class="text-primary">{{ $item->dateRange }}</span></div>
                    </div>
                @endforeach
            </div>
        </section>
        <hr class="m-0" />
        <!-- Skills-->
        <section class="resume-section" id="skills">
            <div class="resume-section-content">
                <h2 class="mb-5">Skills</h2>
                <div class="subheading mb-3">Programming Languages & Tools</div>
                <ul class="list-inline dev-icons">
                    <li class="list-inline-item"><i class="fab fa-laravel"></i></li>
                    <li class="list-inline-item"><i class="fab fa-react"></i></li>
                    <li class="list-inline-item"><i class="fab fa-android"></i></li>
                </ul>
                <div class="subheading mb-3">Workflow</div>
                <ul class="fa-ul mb-0">
                    @foreach ($user->skill->sortDesc() as $item)
                        <li>
                            <span class="fa-li"><i class="fas fa-check"></i></span>
                            {{ $item->skill }}
                        </li>
                    @endforeach
                </ul>
            </div>
        </section>
        <hr class="m-0" />
        <!-- Interests-->
        <section class="resume-section" id="projects">
            <div class="resume-section-content">
                <h2 class="mb-5">Projects</h2>
                @foreach ($user->project->sortDesc() as $item)
                    <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
                        <div class="flex-grow-1">
                            <h3 class="mb-0">{{ $item->name }}</h3>
                            <div class="subheading mb-3">{{ $item->technology }}</div>
                            <p>{{ $item->description }}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </section>
        <hr class="m-0" />
        <!-- Awards-->
        <section class="resume-section" id="awards">
            <div class="resume-section-content">
                <h2 class="mb-5">Awards & Certifications</h2>
                <ul class="fa-ul mb-0">
                    @foreach ($user->award as $item)
                        <li>
                            <span class="fa-li"><i class="fas fa-trophy text-warning"></i></span>
                            {{ $item->award }}
                        </li>
                    @endforeach
                </ul>
            </div>
        </section>
    </div>
</div>
