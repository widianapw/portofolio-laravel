<div>
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Award & Certificate</h6>
    </div>
    <div class="card-body">
        <form enctype="multipart/form-data" wire:submit.prevent="store">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="" class="font-weight-bold">Award</label>
                        <input type="text" wire:model="award.award" placeholder="Award" onfocus="this.placeholder = ''"
                            onblur="this.placeholder = 'Award'" class="form-control">
                        @if ($errors->has('award.award'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('award.award') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">

                </div>

            </div>

            <button class="btn btn-primary">Save</button>
        </form>
        <div class="row mt-5 ml-2 mr-2">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <th>No</th>
                        <th>Award</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        @foreach ($awards as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->award }}</td>
                                <td>
                                    <button class="btn btn-danger"
                                        wire:click="delete({{ $item->id }})">Delete</button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $awards->links() }}
            </div>
        </div>

    </div>
</div>
