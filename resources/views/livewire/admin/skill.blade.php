<div>
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Skill</h6>
    </div>
    <div class="card-body">
        <form enctype="multipart/form-data" wire:submit.prevent="store">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="" class="font-weight-bold">Skill</label>
                        <input type="text" wire:model="skill.skill" placeholder="Skill" onfocus="this.placeholder = ''"
                            onblur="this.placeholder = 'Skill'" class="form-control">
                        @if ($errors->has('skill.skill'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('skill.skill') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">

                </div>

            </div>

            <button class="btn btn-primary">Save</button>
        </form>
        <div class="row mt-5 ml-2 mr-2">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <th>No</th>
                        <th>Skill</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        @foreach ($skills as $item)
                            <tr>

                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    {{ $item->skill }}
                                </td>
                                <td>
                                    <button class="btn btn-danger"
                                        wire:click="delete({{ $item->id }})">Delete</button>
                                </td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $skills->links() }}
            </div>
        </div>

    </div>

</div>
