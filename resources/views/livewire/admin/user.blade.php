<div>
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">User</h6>
    </div>
    <div class="card-body">
        <form enctype="multipart/form-data" wire:submit.prevent="updateUser">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class="font-weight-bold">Image</label>
                        <input type="file" wire:model="image" placeholder="Image" class="form-control">
                        @if ($errors->has('image'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('image') }}</strong>
                            </span>
                        @endif
                    </div>


                    <div wire:loading wire:target="image">
                        <img src="{{ asset('loading/circular.svg') }}" alt="">
                    </div>
                    <div wire:loading.remove wire:target="image">

                        <div class="row justify-content-start align-items-center mt-3">
                            @if ($user->image_url)


                                <div class="col-md-3">
                                    <img src="{{ $user->image_url }}" width="200" class="img-thumbnail">
                                </div>

                            @endif
                            @if ($image)
                                =>
                                <div class="col-md-3">
                                    <img src="{{ $image->temporaryUrl() }}" width="200" class="img-thumbnail">
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class="font-weight-bold">First Name</label>
                        <input type="text" wire:model="user.first_name" placeholder="First Name" class="form-control">
                        @if ($errors->has('user.first_name'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('user.first_name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class="font-weight-bold">Last Name</label>
                        <input type="text" wire:model="user.last_name" placeholder="Last Name" class="form-control">
                        @if ($errors->has('user.last_name'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('user.last_name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class="font-weight-bold">Full Name</label>
                        <input type="text" wire:model="user.full_name" placeholder="Full Name"
                            onfocus="this.placeholder = ''" onblur="this.placeholder = 'Full Name'"
                            class="form-control">
                        @if ($errors->has('user.full_name'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('user.full_name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class="font-weight-bold">Phone</label>
                        <input type="text" wire:model="user.phone" placeholder="Phone" value="{{ $user->phone }}"
                            onfocus="this.placeholder = ''" onblur="this.placeholder = 'Phone'" class="form-control">
                        @if ($errors->has('user.phone'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('user.phone') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class="font-weight-bold">Address</label>
                        <textarea class="form-control" wire:model="user.address" id="" cols="5" rows="5"></textarea>
                        @if ($errors->has('user.address'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('user.address') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class="font-weight-bold">About</label>
                        <textarea class="form-control" wire:model="user.about" id="" cols="5" rows="5"></textarea>
                        @if ($errors->has('user.about'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('user.about') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class="font-weight-bold">Facebook Url</label>
                        <input type="text" wire:model="user.facebook_url" placeholder="Facebook Url"
                            class="form-control">
                        @if ($errors->has('user.facebook_url'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('user.facebook_url') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class="font-weight-bold">Twitter Url</label>
                        <input type="text" wire:model="user.twitter_url" placeholder="Twitter Url" class="form-control">
                        @if ($errors->has('user.twitter_url'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('user.twitter_url') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class="font-weight-bold">Github Url</label>
                        <input type="text" wire:model="user.github_url" placeholder="Github Url" class="form-control">
                        @if ($errors->has('user.github_url'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('user.github_url') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class="font-weight-bold">Linked In Url</label>
                        <input type="text" wire:model="user.linked_in_url" placeholder="Linked In Url"
                            class="form-control">
                        @if ($errors->has('user.linked_in_url'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('user.linked_in_url') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row mt-5">
                <div class="col-md-12">
                    <button class="btn btn-primary btn-block">Save Change</button>
                </div>
            </div>
        </form>
    </div>
</div>
