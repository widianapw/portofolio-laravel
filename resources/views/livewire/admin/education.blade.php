<div>
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Educations</h6>
    </div>
    <div class="card-body">
        <form enctype="multipart/form-data" wire:submit.prevent="store">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="" class="font-weight-bold">Organization</label>
                        <input type="text" wire:model="education.organization" placeholder="Organization"
                            class="form-control">
                        @if ($errors->has('education.organization'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('education.organization') }}</strong>
                            </span>
                        @endif

                        <label for="" class="font-weight-bold mt-3">Majority</label>
                        <input type="text" wire:model="education.majority" placeholder="Majority" class="form-control">
                        @if ($errors->has('education.majority'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('education.majority') }}</strong>
                            </span>
                        @endif

                        <label for="" class="font-weight-bold mt-3">Date From</label>
                        <input type="date" wire:model="education.date_from" placeholder="Date From"
                            class="form-control">
                        @if ($errors->has('education.date_from'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('education.date_from') }}</strong>
                            </span>
                        @endif

                        <label for="" class="font-weight-bold mt-3">Date Until</label>
                        <div class="form-group">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" wire:model="is_present" class="custom-control-input"
                                    id="customSwitch1">
                                <label class="custom-control-label" for="customSwitch1">Set Until Present</label>
                            </div>
                        </div>
                        @if (!$is_present)
                            <input type="date" wire:model="education.date_until" placeholder="Date Until"
                                class="form-control">
                            @if ($errors->has('education.date_until'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $errors->first('education.date_until') }}</strong>
                                </span>
                            @endif
                        @endif

                    </div>
                </div>
                <div class="col-md-6">

                </div>

            </div>

            <button class="btn btn-primary">Save</button>
        </form>
        <div class="row mt-5 ml-2 mr-2">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <th>No</th>
                        <th>Organization</th>
                        <th>Majority</th>
                        <th>From</th>
                        <th>Until</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        @foreach ($educations as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->organization }}</td>
                                <td>{{ $item->majority }}</td>
                                <td>{{ $item->date_from }}</td>
                                <td>{{ $item->date_until ?? 'Present' }}</td>
                                <td>
                                    <button class="btn btn-danger"
                                        wire:click="delete({{ $item->id }})">Delete</button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $educations->links() }}
            </div>
        </div>

    </div>
</div>
