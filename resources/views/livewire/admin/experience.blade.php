<div>
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Experiences</h6>
    </div>
    <div class="card-body">
        <form enctype="multipart/form-data" wire:submit.prevent="store">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="" class="font-weight-bold">Position</label>
                        <input type="text" wire:model="experience.position" placeholder="Position" class="form-control">
                        @if ($errors->has('experience.position'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('experience.position') }}</strong>
                            </span>
                        @endif

                        <label for="" class="font-weight-bold mt-3">Company</label>
                        <input type="text" wire:model="experience.company" placeholder="Company" class="form-control">
                        @if ($errors->has('experience.company'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('experience.company') }}</strong>
                            </span>
                        @endif

                        <label for="" class="font-weight-bold mt-3">Description</label>
                        <input type="text" wire:model="experience.description" placeholder="description"
                            class="form-control">
                        @if ($errors->has('experience.description'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('experience.description') }}</strong>
                            </span>
                        @endif

                        <label for="" class="font-weight-bold mt-3">Date From</label>
                        <input type="date" wire:model="experience.date_from" placeholder="Date From"
                            class="form-control">
                        @if ($errors->has('experience.date_from'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('experience.date_from') }}</strong>
                            </span>
                        @endif

                        <label for="" class="font-weight-bold mt-3">Date Until</label>
                        <div class="form-group">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" wire:model="is_present" class="custom-control-input"
                                    id="customSwitch1">
                                <label class="custom-control-label" for="customSwitch1">Set Until Present</label>
                            </div>
                        </div>
                        @if (!$is_present)
                            <input type="date" wire:model="experience.date_until" placeholder="Date Until"
                                class="form-control">
                            @if ($errors->has('experience.date_until'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $errors->first('experience.date_until') }}</strong>
                                </span>
                            @endif
                        @endif
                    </div>
                </div>
                <div class="col-md-6">

                </div>

            </div>

            <button class="btn btn-primary">Save</button>
        </form>
        <div class="row mt-5 ml-2 mr-2">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <th>No</th>
                        <th>Position</th>
                        <th>Company</th>
                        <th>Description</th>
                        <th>From</th>
                        <th>Until</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        @foreach ($experiences as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->position }}</td>
                                <td>{{ $item->company }}</td>
                                <td>{{ $item->description }}</td>
                                <td>{{ $item->date_from }}</td>
                                <td>{{ $item->date_until ?? 'Present' }}</td>
                                <td>
                                    <button class="btn btn-danger"
                                        wire:click="delete({{ $item->id }})">Delete</button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $experiences->links() }}
            </div>
        </div>

    </div>
</div>
