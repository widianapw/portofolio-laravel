<div>
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Projects</h6>
    </div>
    <div class="card-body">
        <form enctype="multipart/form-data" wire:submit.prevent="store">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="" class="font-weight-bold">Project Name</label>
                        <input type="text" wire:model="project.name" placeholder="Project Name" class="form-control">
                        @if ($errors->has('project.name'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('project.name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="" class="font-weight-bold">Technology Build</label>
                        <input type="text" wire:model="project.technology" placeholder="Technology Build"
                            class="form-control">
                        @if ($errors->has('project.technology'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('project.technology') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="" class="font-weight-bold">Description</label>
                        <input type="text" wire:model="project.description" placeholder="Description"
                            class="form-control">
                        @if ($errors->has('project.description'))
                            <span class="invalid-feedback" style="display: block;" role="alert">
                                <strong>{{ $errors->first('project.description') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>

            <button class="btn btn-primary mt-3">Save</button>
        </form>
        <div class="row mt-5 ml-2 mr-2">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <th>No</th>
                        <th>Project Name</th>
                        <th>Technology</th>
                        <th>Description</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        @foreach ($projects as $item)
                            <tr>

                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    {{ $item->name }}
                                </td>
                                <td>
                                    {{ $item->technology }}
                                </td>
                                <td>
                                    {{ $item->description }}
                                </td>
                                <td>
                                    <button class="btn btn-danger"
                                        wire:click="delete({{ $item->id }})">Delete</button>
                                </td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $projects->links() }}
            </div>
        </div>

    </div>

</div>
