<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center">
        <div class="sidebar-brand-text mx-3">Admin</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">
    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Data
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="{{ route('admin.user') }}" aria-expanded="true"
            aria-controls="collapseTwo">
            <span>User</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="{{ route('admin.education') }}" aria-expanded="true"
            aria-controls="collapseTwo">

            <span>Education</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="{{ route('admin.experience') }}" aria-expanded="true"
            aria-controls="collapseTwo">

            <span>Experience</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="{{ route('admin.skill') }}" aria-expanded="true"
            aria-controls="collapseTwo">

            <span>Skill</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="{{ route('admin.project') }}" aria-expanded="true"
            aria-controls="collapseTwo">

            <span>Project</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="{{ route('admin.award') }}" aria-expanded="true"
            aria-controls="collapseTwo">

            <span>Award</span>
        </a>
    </li>

</ul>
<!-- End of Sidebar -->
